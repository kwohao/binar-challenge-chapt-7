"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class game_room extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      game_room.belongsTo(models.user, {
        foreignKey: "player1_id",
        as: "player1",
      });
      game_room.belongsTo(models.user, {
        foreignKey: "player2_id",
        as: "player2",
      });
    }
  }
  game_room.init(
    {
      nama_room: DataTypes.STRING,
      player1_id: DataTypes.STRING,
      player1_choice: DataTypes.STRING,
      player1_result: DataTypes.STRING,
      player2_id: DataTypes.STRING,
      player2_choice: DataTypes.STRING,
      player2_result: DataTypes.STRING,
      status_room: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "game_room",
    }
  );
  return game_room;
};
