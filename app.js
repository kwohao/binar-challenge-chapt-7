const express = require("express");
const app = express();
const userRouter = require("./users/user.route");
const PORT = 3001;
// const cors = require("cors");
// const swaggerUi = require("swagger-ui-express");
// const gameSwagger = require("./gameSwagger.json");
require("dotenv").config();

// app.use(cors());
app.use(express.json());
app.use(express.static("public"));

// app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(gameSwagger));

// app.get("/", (req, res) => {
//   res.sendFile("index.html");
// });

app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.get("/about", (req, res) => {
  res.send("This is my about route..... ");
});

app.use("/user", userRouter);

app.listen(PORT, () => {
  console.log(`app running at port ${PORT}`);
});

module.exports = app;
