const userModel = require("./user.model");
const jwt = require("jsonwebtoken");
const { validationResult } = require("express-validator");
const judgementRPS = require("../utils/judgementValidation");
require("dotenv").config();
// const { all } = require("./route");
class UserController {
  getAllUser = async (req, res) => {
    try {
      const allUser = await userModel.getAllUser();
      return res.json(allUser);
    } catch (error) {
      console.log(error);
      res.statusCode = 500;
      return res.json({ message: "Internal Server Error" });
    }
  };

  registerUser = async (req, res) => {
    // membuat agar userData dapat diproses di body raw json
    const userData = req.body;
    //validasi agar saat data yang dimasukan tidak sesuai, tidak mau merekam
    try {
      const existUser = await userModel.isUserExist(userData);
      if (existUser) {
        res.statusCode = 400;
        return res.json({ message: "Username atau Email sudah terdaftar" });
      }

      userModel.registerNewUser(userData);
      return res.json({ message: "User Baru telah Terdaftar" });
    } catch (error) {
      res.statusCode = 400;
      console.log(error);
      return res.json({ message: "ada error di controller registerUser" });
    }
  };

  loginUser = async (req, res) => {
    const { username, password } = req.body;
    try {
      const dataLogin = await userModel.loginUser(username, password);
      console.log(dataLogin);
      if (dataLogin) {
        const token = jwt.sign(
          { ...dataLogin, role: "player" },
          process.env.SECRET_KEY,
          { expiresIn: "1d" }
        );

        return res.json({ accessToken: token });
      } else {
        return res.status(400).json({ message: "User tidak ditemukan" });
      }
    } catch (error) {
      console.log(error);
      return res.status(400).json({ message: "User tidak ditemukan" });
    }
  };

  findUser = async (req, res) => {
    const { userId } = req.params;
    try {
      const dataUser = await userModel.findUser(userId);
      return res.json({ dataUser });
    } catch (error) {
      return res
        .status(400)
        .json({ message: " ada yang salah di controller findUser" });
    }
  };

  updateUserBio = async (req, res) => {
    // dapatkan userId dari param
    const { userId } = req.params;
    const { fullname, address, phoneNumber, age } = req.body;
    // pastikan data yang dimasukan sesuai syarat

    //cek apakah data sudah ada atau belum
    const bioExist = await userModel.isUserBioExist(userId);
    try {
      if (bioExist) {
        // jika userId sudah terdaftar update biodata
        userModel.updateUserBio(userId, fullname, address, phoneNumber, age);
        return res.json({ message: "Biodata sudah di update" });
      } else {
        // jika userId tidak ada di db buatkan biodata baru
        const data = await userModel.registerUserBio(
          userId,
          fullname,
          address,
          phoneNumber,
          age
        );
        return res.json({ message: "Biodata sudah di buat" });
      }
    } catch (error) {
      res.statusCode = 400;
      res.json({ message: "error try catch controller updateUserBio" });
    }
  };

  findUserBio = async (req, res) => {
    // dapatkan userId dari params
    const { userId } = req.params;
    try {
      const dataUser = await userModel.findUserBio(userId);
      if (dataUser === null) {
        res.statusCode = 404;
        return res.json({ message: "User tidak ditemukan" });
      } else {
        return res.json({ dataUser });
      }
    } catch (error) {
      res.statusCode = 500;
      return res.json({ message: "Internal Server Error" });
    }
  };

  inputHistory = async (req, res) => {
    const userGame = req.body;
    const { userId } = req.params;
    const user = await userModel.findUserBio(userId);
    try {
      if (!user) {
        return res.status(400).json({ message: "User tidak ditemukan" });
      }

      userModel.inputHistory(userGame, userId);
      return res.json({ message: "data recorded" });
    } catch (error) {
      res.statusCode = 400;
      return res.json({ message: "Masukan data dengan benar" });
    }
  };

  getUserHistory = async (req, res) => {
    // pastikan userId terekem sebagai parameter
    const { userId } = req.params;
    try {
      const userHistory = await userModel.getUserHistory(userId);
      return res.json(userHistory);
    } catch (error) {
      console.log(error);
      res.statusCode = 500;
      return res.json({ message: "Internal Server Error" });
    }
  };

  //  ================================================== GAME ==============================================

  createRoom = async (req, res) => {
    const player1 = req.body;
    const token = req.token.id;

    try {
      if (
        player1.playerOneChoice !== "batu" &&
        player1.playerOneChoice !== "kertas" &&
        player1.playerOneChoice !== "gunting"
      ) {
        return res
          .status(400)
          .json({ message: "Format harus berupa batu / kertas / gunting" });
      }

      await userModel.createRoom(player1, token);
      return res.json({ message: "room berhasil di buat" });
    } catch (error) {
      console.log(error);
      return res
        .status(400)
        .json({ message: "ada yang salah di controller createRoom" });
    }
  };

  // joinRoom = async (req, res) => {
  //   const player2 = req.body;
  //   const token = req.token.id;
  //   const { id } = req.params;

  //   try {
  //     // cari room berdasarkan ID
  //     const singleRoom = await userModel.getSingleRoom(id);

  //     // buat agar player tidak melawan dirinya sendiri
  //     if (singleRoom.player1_id === token) {
  //       res.statusCode = 400;
  //       return res.json({ message: "Dilarang melawan diri sendiri" });
  //     }

  //     // buat agar player tidak dapat bermain atau mengganti data di room yang sudah selesai
  //     if (singleRoom.status_room === "Finish") {
  //       res.statusCode = 400;
  //       return res.json({ message: "Room sudah selesai" });
  //     }
  //     // input data pilihan player 2 ke database
  //     await userModel.joinRoom(player2, token, id);

  //     const p1Result = singleRoom.player1_choice;
  //     const p2Result = singleRoom.player2_choice;

  //     // tentukan pemenang
  //     const result = await judgementRPS.judgementRPS(p1Result, p2Result);

  //     // masukan result pada database
  //     const RoomResult = await userModel.RoomResult(result, id);
  //     return res.json(RoomResult);
  //   } catch (error) {
  //     console.log(error);
  //     return res
  //       .status(500)
  //       .json({ message: "ada yang salah di controller joinRoom" });
  //   }
  // };

  joinRoom = async (req, res) => {
    const player2 = req.body;
    const token = req.token.id;
    const { id } = req.params;

    try {
      // cari room berdasarkan ID
      const singleRoom = await userModel.getSingleRoom(id);

      // buat agar player tidak melawan dirinya sendiri
      if (singleRoom.player1_id === token) {
        res.statusCode = 400;
        return res.json({ message: "Dilarang melawan diri sendiri" });
      }

      // buat agar player tidak dapat bermain atau mengganti data di room yang sudah selesai
      if (singleRoom.status_room === "Finish") {
        res.statusCode = 400;
        return res.json({ message: "Room sudah selesai" });
      }

      // input data pilihan player 2 ke database
      await userModel.joinRoom(player2, token, id);

      const p1Result = singleRoom.player1_choice;
      const p2Result = singleRoom.player2_choice;

      // tentukan pemenang
      const result = await judgementRPS.judgementRPS(p1Result, p2Result);

      // masukan result pada database
      const RoomResult = await userModel.RoomResult(result, id);
      res.json({ message: "Berhasil melawan " });
      return RoomResult;
    } catch (error) {
      console.log(error);
      return res
        .status(500)
        .json({ message: "ada yang salah di controller joinRoom" });
    }
  };

  getAllRoom = async (req, res) => {
    try {
      const allRooms = await userModel.getAllRoom();
      return res.json(allRooms);
    } catch (error) {
      return res
        .status(500)
        .json({ message: "ada kesalahan di controller getAllRoom" });
    }
  };

  getSingleRoom = async (req, res) => {
    const { id } = req.params;
    try {
      const singleRoom = await userModel.getSingleRoom(id);
      return res.json(singleRoom);
    } catch (error) {
      console.log(error);
      return res
        .status(500)
        .json({ message: "ada yang salah di controller getSingleRoom" });
    }
  };

  singleUserHistory = async (req, res) => {
    const id = req.token.id;
    try {
      const userHistory = await userModel.singleUserHistory(id);
      return res.json(userHistory);
    } catch (error) {
      return res
        .status(500)
        .json({ message: "ada error di controller singleUserHistory" });
    }
  };

  playerVsCom = async (req, res) => {
    const id = req.token.id;
    const { player1_choice } = req.body;

    try {
      // create room dan rekam pilihan player 1
      const nama_room = "VS COM";
      const player2_choice = judgementRPS.vsCom();
      const [player1_result, player2_result] = judgementRPS.judgementRPS(
        player1_choice,
        player2_choice
      );

      // buat agar inputan hanya berupa batu kertas atau gintung
      if (
        player1_choice !== "batu" &&
        player1_choice !== "kertas" &&
        player1_choice !== "gunting"
      ) {
        return res
          .status(400)
          .json({ message: "Format harus berupa batu / kertas / gunting" });
      }

      await userModel.playerVsCom(
        nama_room,
        id,
        player1_choice,
        player1_result,
        player2_choice,
        player2_result
      );
      return res.json({ message: "berhasil membuat room playerVsCom" });
    } catch (error) {
      console.log(error);
      return res
        .status(400)
        .json({ message: "pilihan harus berupa batu / kertas / gunting" });
    }
  };
}

module.exports = new UserController();
