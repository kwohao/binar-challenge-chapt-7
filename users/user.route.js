const express = require("express");
const userRouter = express.Router();
const UserController = require("./user.controller");
const authMiddleware = require("../authMiddleware/authMiddleware");
const protectionMiddleware = require("../authMiddleware/protectionMiddleware");
const registrationValidator = require("./registrationValidator");
const validationMiddleware = require("../authMiddleware/validationMiddleware");
const userController = require("./user.controller");

userRouter.get("/alluser", UserController.getAllUser);

userRouter.post(
  "/regis",
  registrationValidator.userRegisterValidation,
  validationMiddleware,
  UserController.registerUser
);

userRouter.post(
  "/login",
  registrationValidator.userLoginValidation,
  validationMiddleware,
  UserController.loginUser
);

userRouter.get(
  "/singleUser/:userId",
  authMiddleware,
  protectionMiddleware,
  userController.findUser
);

userRouter.put(
  "/updatebio/:userId",
  authMiddleware,
  protectionMiddleware,
  registrationValidator.updateBioValidation,
  validationMiddleware,
  UserController.updateUserBio
);

userRouter.get(
  "/detail/:userId",
  authMiddleware,
  protectionMiddleware,
  UserController.findUserBio
);

userRouter.post(
  "/inputHistory/:userId",
  authMiddleware,
  protectionMiddleware,
  registrationValidator.inputHistory,
  validationMiddleware,
  UserController.inputHistory
);

userRouter.get("/history/:userId", UserController.getUserHistory);

// =========================================== GAME =====================================

// API create room by player1
userRouter.post(
  "/createRoom",
  authMiddleware,
  registrationValidator.createRoom,
  validationMiddleware,
  UserController.createRoom
);

// API join room by player2
userRouter.put(
  "/joinRoom/:id",
  authMiddleware,
  registrationValidator.joinRoom,
  validationMiddleware,
  UserController.joinRoom
);

// API untuk mendapatkan semua room / Lobby
userRouter.get("/allRooms", authMiddleware, UserController.getAllRoom);

// API untuk mendapatkan info sebuah room
userRouter.get("/singleRoom/:id", authMiddleware, UserController.getSingleRoom);

// API memanggil history setiap user beradasarkan id dari token.id
userRouter.get("/pvpHistory", authMiddleware, userController.singleUserHistory);

// API create room vs Com tambahkan di bagian message agar player mengetahui hasil permainan melawan com
// jadikan player2_id = Vs Com
userRouter.post(
  "/vsCom",
  authMiddleware,
  registrationValidator.vsCom,
  validationMiddleware,
  userController.playerVsCom
);

module.exports = userRouter;
